## Have I mentioned that I don't like markdown at all?
Yes.

## What is this and why do I care?
Believe it or not, more than a decade later, that huge method for parsing input to Noodle still wakes me up, screaming, in the middle of the night. It's ruined relationships.

This is some code to replace that method. It might be more LOC than the method, but:
  1. It gets rid of a huge conditional and conditionals are the worst.
  1. far less assignment going on
  1. It should be more isolated and easier to maintain/extend
  1. It's more OO


Let us start at the beginning. I noticed on https://github.com/borusg/noodle/blob/main/lib/noodle/controller.rb that Noodle is a top-level class and it should almost certainly be a module. Just a quibble, perhaps but worth the change, I think. Classes should do one thing. Modules are for grouping code.

The code found herein is designed to replace some or all of #self.magic which, at the time of writing, begins on line 48 and ends somewhere past the James Webb telescope's current location.

NB: I kinda remember how Noodle works, but not really so I likely got some stuff wrong and also realized you might not even want this code so I got pretty lazy towards the end and schpackled some stuff just to hopefully get the idea across without going full-boar because you know I don't realllly remember.

It's been a while.

Anyway! Maybe just `bundle install && bundle exec ruby noddlin.rb` and then let's talk InputParser.

Here's some resources to help you understand what's happening. When I wrote my first Parslet::Parser I was afraid, angry, hungry, frustrated and nonplussed, but I promise it's pretty easy to understand.

1. https://kschiess.github.io/parslet/
1. http://florianhanke.com/blog/2011/02/01/parslet-intro.html

I wrote my first one almost 3 years ago and didn't remember it at all about 4 hours ago when I started this adventure. I am at one with the me who is on this adventure! Those two documents were all I needed to get this rolling and if I can understand it, I'm sure you can understand it.

There might be better ways to do this, etc. The :clauses rule is the thing that calls all of the other rules and figures out what your input was. I wish there was a way to meta-program it, and maybe there is, but I couldn't find one. Note that order is important here! Mostly I think term has to come at the end. '>>' means 'followed by' and pipe means 'or' and I think the rest is kinda obvi, but lmk if not.

Around line 83 I started building up examples/test and if I was a real Ruby prog, I'd write real tests, but I'll never be a real boy.... at least not today. I put them in an array and shuffled it to make sure that it still worked if one of the "terminates with an equals sign" (or the like) entries was at the end with no trailing space.

Uncomment any/all of those pretty prints to screw around with it. If you don't like my code that consumes that output, you might like that output.

Moving on to Noodle.parse_user_input. I haven't really reviewed the current state of the codebase, so there might be and hopefully is a better place for this, but I just stuck it here for convenience. Convenience is very inconvenient to type.

As you know, I can't stand using primitives (like Hash) for anything other than message passing to constructors. I mean, we all know this and we all agree so I'm not sure why I mentioned it other than to say that primitives are horrible.

While some passive voice could be used and some code could be constructed to deal with the hash(es) printed out from those pretty print lines, I thought I'd demo the Parslet Transformer class that IMO is pretty cool. That's what self.parse_user_input is for. It creates the ast and then runs it through the transformer which then gives you real honest-to-jeez objects to work with instead of hash junk.

module Clause is all about these transformations. You need some way to get between a clause identifier like ':key_equals' and a Ruby class and I did that by putting the clause processors in the Processors module so that I can look them up dynamically. If this is confusing, please let me know. There are other ways to do it, but I think this one has the advantage of, if you make a new clause type, you just have to make a new class that conforms to the API of those (having a #self.for method to match it up to its input and having #operator and #to_query) and without changing anything else, you've got a new clause type. This is an example of the O in SOLID. It's also using the Null Object pattern with the Catchall class that I wrote because I'm lazy and didn't want to write all of the classes I wrote clauses for. Using this pattern is an example of S and L in SOLID. In your real code you'd of course write classes for everybody.

KVEqual is the buddy :key_equals and I faked some Elasticsearch query syntax because it's been 3 years since I've touched that too. Hopefully the point comes across. Also, when I was writing my PEG it was for doing search-engine type shit so :must and :should, etc were valid for me and I'm guessing won't be as valid for you but perhaps it turns some lights on or something. The goal here is to have a very tiny amount of code that deals with the key_equals input data. So small that it's difficult to have bugs in it. The code is also isolated from the other code, so modifying it if necessary won't be as likely to have side-effects unexpected or otherwise.

The Query class just zipps all that shit together and assembles the query. Again, I faked this and it almost certainly doesn't produce valid ESQL, but again, hopefully the point comes across.

Aren't you glad this email wasn't a meeting.
