require 'parslet'
require 'pp'

module Noodle
class InputParser < Parslet::Parser

  rule(:clause) do
    ( term_and_show | format | value_for_key_matches_regex | distinct_values | sum_values | key_equals | key_not_equal | values_for_key | negated_term | term).as(:clause)
  end

  rule(:distinct_values) do
    (str(':') >> term).as(:distinct_values)
  end

  rule(:equals) do
    str('=')
  end

  rule(:format) do
    (str('json') | str('json_params_only') | str('full')).as(:format)
  end

  rule(:key_equals) do
    ((term).as(:key) >> equals >> (term).as(:value)).as(:key_equals)
  end

  rule(:key_not_equal) do
    ((negated_term).as(:key) >> equals >> (term).as(:value)).as(:key_not_equal)
  end

  rule(:negated_term) do
    ((str('@') | str('-')) >> term).as(:negated_term)
  end

  rule(:not_space) do
    match(/\S/)
  end

  rule(:not_special) do
    match(/[^\s:@?=+-]/)
  end

  rule(:query) do
    (space? >> clause >> space?).repeat.as(:query)
  end

  rule(:questionmark) do
    str('?')
  end

  rule(:space) do
    match(/\s/).repeat(1)
  end

  rule(:space?) do
    space.maybe
  end

  rule(:sum_values) do
    (term >> str('+')).as(:sum_values)
  end

  rule(:term) do
    not_special.repeat(1).as(:term)
  end

  rule(:term_and_show) do
    (space? >> term >> ( questionmark >> equals | equals >> questionmark ) >> space?).as(:term_and_show)
  end

  rule(:values_for_key) do
    (term >> str('=')).as(:values_for_key)
  end

  rule(:value_for_key_matches_regex) do
    (term >> str('=~') >> (not_space.repeat(1).as('regex')) >> space?).as(:value_for_key_matches_regex)
  end

  root(:query)
end
end

input = %w{json site?= -notme @not=meeither france=bacon yousaidalligator= reggie=~watts :truedistinction notall+ hostnameIguess}.shuffle.join(' ')
pp Noodle::InputParser.new.parse(input)
#pp Noodle::InputParser.new.parse(%q{json site?= -notme @not=meeither yousaidalligator= reggie=~watts :truedistinction notall+})
#pp Noodle::InputParser.new.parse(%q{site?= -notme @not=meeither yousaidalligator= reggie=~watts :truedistinction notall+ full})
#pp Noodle::InputParser.new.parse(%q{site?= -notme @not=meeither yousaidalligator= reggie=~watts :truedistinction notall+})
#pp Noodle::InputParser.new.parse(%q{site?= -notme @not=meeither yousaidalligator= reggie=~watts :truedistinction notall+})
#pp Noodle::InputParser.new.parse(%q{site?= -notme @not=meeither yousaidalligator= reggie=~watts :truedistinction})
#pp Noodle::InputParser.new.parse(%q{site?= -notme @not=meeither yousaidalligator= reggie=~watts})
#pp Noodle::InputParser.new.parse(%q{site?= -notme @not=meeither})
#pp Noodle::InputParser.new.parse(%q{site?= -notme})
#pp Noodle::InputParser.new.parse(%q{site })
#pp Noodle::InputParser.new.parse(%q{site?= })
#pp Noodle::InputParser.new.parse(%q{site?=})
#pp Noodle::InputParser.new.parse(%q{site?= term})
#pp Noodle::InputParser.new.parse(%q{site=? })

module Noodle
  def self.parse_user_input(user_input)
    begin
      parse_tree = InputParser.new.parse(user_input.to_s)
    rescue Parslet::ParseFailed => e
      puts "you gave me a walnut."
      puts e.parse_failure_cause.ascii_tree
      exit(1)
    end
    query = RosyGlassesPrime.new.apply(parse_tree)
    pp query
  end

  module Clause

    def self.create(ast_clause)
      type = ast_clause.keys.first
      processor_for(type).new(ast_clause[type])
    end
    def self.processor_for(type)
      proc_sym = Processors.constants.find{|cnst| Processors.const_get(cnst).for == type} || :Catchall
      Processors.const_get(proc_sym)
    end
    module Processors
      class Catchall
        def self.for
          :the_movie_star_and_the_rest
        end
        def initialize(ast_hash)
          @ast_hash = ast_hash
        end
        def operator
          :should
        end

        def to_query
          {
            just: 'an',
            example: 'jeez',
          }
        end
      end
      class KVEqual
        def self.for
          :key_equals
        end
        def initialize(ast_hash)
          @key = ast_hash[:key][:term]
          @value = ast_hash[:value][:term]
        end
        def operator
          :must
        end
        def to_query
          {
            match: {
              field: @key,
              query: @value,
            }
          }
        end
      end
      class KVNotEqual < KVEqual
        def self.for
          :key_not_equal
        end
        def initialize(ast_hash)
          @key = ast_hash[:key][:negated_term][:term]
          @value = ast_hash[:value][:term]
        end
        def operator
          :must_not
        end
      end
    end
  end

  class Query
    def initialize(clauses)
      @input_clauses = clauses
    end
    def to_elasticsearch
      query = {
        query: {
          bool: {}
        }
      }
      query[:query][:bool][:must_not] = must_not_terms.map(&:to_query) unless must_not_terms.empty?
      query[:query][:bool][:must]     = must_terms.map(&:to_query)     unless must_terms.empty?
      query[:query][:bool][:should]   = should_terms.map(&:to_query)   unless should_terms.empty?
      query
    end

    private

    def clauses
      @clauses ||= @input_clauses.group_by{|clause| clause.operator}
    end
    def must_not_terms
      @must_not_terms ||= clauses.fetch(:must_not,[])
    end
    def must_terms
      @must_terms ||= clauses.fetch(:must,[])
    end
    def should_terms
      @should_terms ||= clauses.fetch(:should,[])
    end
  end
  # This is an OptimistPrime/Transformers multi-layered joke. Please do not attempt.
  class RosyGlassesPrime < Parslet::Transform
    rule(clause: subtree(:clause)) do
      Clause.create(clause)
    end
    rule(query: sequence(:clauses)) do
      Query.new(clauses)
    end
  end
end
x = Noodle.parse_user_input(input)
pp x
puts x.to_elasticsearch
